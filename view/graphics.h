#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <QMouseEvent>
#include <QWheelEvent>
#include <QKeyEvent>
#include <iostream>
#include <QOpenGLWidget>

class Graphics : public QOpenGLWidget
{
public:

    Graphics();
    ~Graphics();

    void paintGL() override {

    }

    void paintEvent(QPaintEvent* e) override {

    }

    void mouseMoveEvent(QMouseEvent* event) override {
        std::cout << "Move\t" << event->pos().x() << "\t" << event->pos().y() << "\n";
    }
    void mouseDoubleClickEvent(QMouseEvent* event) override {
        std::cout << "Double Click\t" << event->pos().x() << "\t" << event->pos().y() << "\n";
    }
    void mousePressEvent(QMouseEvent* event) override {
        std::cout << "Press\t" << event->pos().x() << "\t" << event->pos().y() << "\n";
    }
    void mouseReleaseEvent(QMouseEvent* event) override {

    }
    void wheelEvent(QWheelEvent* event) override {}
    void keyPressEvent(QKeyEvent* event) override {}
    void keyReleaseEvent(QKeyEvent* event) override {}


};

#endif // GRAPHICS_H
