#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    grid_layout_= new QGridLayout();
    graphics_ = new Graphics();
    button_ = new QPushButton("hey");
    graphics_->update();
    grid_layout_->addWidget(graphics_, 0, 0, 1, 1);
    grid_layout_->addWidget(button_, 0, 1, 2, 2);
    QWidget *qw = new QWidget();
    qw->setLayout(grid_layout_);
    this->setCentralWidget(qw);
}

MainWindow::~MainWindow() {}
