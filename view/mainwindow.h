#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGridLayout>
#include <memory>
#include "graphics.h"
#include <QPushButton>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    QGridLayout* grid_layout_;
    QGraphicsView* graphics_;
    QPushButton* button_;
};

#endif // MAINWINDOW_H
