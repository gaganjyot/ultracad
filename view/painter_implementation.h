#ifndef PAINTER_IMPLEMENTATION_H
#define PAINTER_IMPLEMENTATION_H

#include "../kernel/interface/painter.h"
#include <QGraphicsScene>
#include <memory>

struct painter_impl : public interface::painter {

    painter_impl() {
        scene_ = std::make_unique<QGraphicsScene>();
    }

    void set_color(uint8_t red, uint8_t green, uint8_t blue, uint8_t alpha = 0) override {
        pen_.setColor(QColor(red, gree, blue, alpha));
    }

    void set_thickness(uint8_t thickness) override {
        pen_.setWidth(width);
    }

    void draw_line(double x1, double y1, double x2, double y2) override {
        scene_->addLine(x1, y1, x2, y2, pen_);
    }

    void draw_circle(double x, double y, double radius) override {

    }

    void draw_arc(double x, double y, double radius, double start_angle, double end_angle) override {

    }

    void draw_point(double x, double y) override {

    }
private:
    std::unique_ptr<QGraphicsScene> scene_;
    QPen pen_;
};

#endif // PAINTER_IMPLEMENTATION_H
