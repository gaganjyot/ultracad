#ifndef SNAP_HANDLER_H
#define SNAP_HANDLER_H

#include <vector>
#include "util/coordinate.h"

struct snap_handler {
    snap_handler() {}
    void toggle_end_point() {
        snap_end_point_ = !snap_end_point_;
    }
    void toggle_mid_point() {
        snap_mid_point_ = !snap_mid_point_;
    }
    void toggle_center() {
        snap_center_ = !snap_center_;
    }
    void toggle_geo_center() {
        snap_geo_center_ = !snap_geo_center_;
    }
    void toggle_quadrant() {
        snap_quadrant_ = !snap_quadrant_;
    }
    void toggle_tangent() {
        snap_tangent_ = !snap_tangent_;
    }
    void toggle_intersection() {
        snap_intersection_ = !snap_intersection_;
    }
    void toggle_nearest() {
        snap_nearest_ = !snap_nearest_;
    }

    std::vector<util::coordinate> snap_points() {
        std::vector<util::coordinate> points;
        if(snap_end_point_) {

        }
        if(snap_mid_point_) {

        }
        if(snap_center_) {

        }
        if(snap_geo_center_) {

        }
        if(snap_quadrant_) {

        }
        if(snap_tangent_) {

        }
        if(snap_intersection_) {

        }
        if(snap_nearest_) {

        }
        return points;
    }

    ~snap_handler() {}

private:
    bool snap_end_point_;
    bool snap_mid_point_;
    bool snap_center_;
    bool snap_geo_center_;
    bool snap_quadrant_;
    bool snap_tangent_;
    bool snap_intersection_;
    bool snap_nearest_;
};

#endif // SNAP_HANDLER_H
