#ifndef INTERSECTION_HANDLER_H
#define INTERSECTION_HANDLER_H

#include "util/coordinate.h"
#include "maths/equations.h"
#include <vector>

namespace math {
using cd_vec = std::vector<util::coordinate>;

class intersection_handler {
    static cd_vec linear_linear(linear& l, linear& l) {}
    static cd_vec linear_quadratic(linear& l, quadratic& q) {}
    static cd_vec quadratic_quadratic(quadratic& q, quadratic& q) {}
};
}

#endif // INTERSECTION_HANDLER_H
