#ifndef COLOR_H
#define COLOR_H

#include <cstdint>

struct color {
    color(uint8_t r = 0, uint8_t g = 0, uint8_t b = 0, uint8_t a = 0)
        : r_(r), b_(b), g_(g), a_(a) {}

    uint8_t red() const { return r_; }
    uint8_t green() const { return g_; }
    uint8_t blue() const { return b_; }
    uint8_t alpha() const { return a_; }
private:
    uint8_t r_, g_, b_, a_;
};

#endif // COLOR_H
