#ifndef GENERIC_VISITOR_H
#define GENERIC_VISITOR_H

#include <memory>
#include <vector>
#include "entities/circle.h"
#include "util/coordinate.h"

class center_process {
    void process(std::shared_ptr<circle> c, std::vector<util::coordinate>& vec) {
        vec.push_back(c->center());
    }

    void process()
private:

};


#endif // GENERIC_VISITOR_H
