#ifndef BOUNDING_BOX_H
#define BOUNDING_BOX_H

#include "coordinate.h"

namespace util {
struct area {

    area(const coordinate& bottom_left = coordinate{0, 0}, const coordinate& top_right = coordinate{0, 0})
        : bottom_left_{bottom_left}, top_right_{top_right} {

    }

    area(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y)
        : bottom_left_{bottom_left_x, bottom_left_y},
          top_right_{top_right_x, top_right_y} {
    }

    double height() const {
        return top_right_.y() - bottom_left_.y();
    }

    double width() const {
        return top_right_.x() - bottom_left_.y();
    }

    coordinate bottom_left() const {
        return bottom_left_;
    }

    coordinate top_right() const {
        return top_right_;
    }

    area merge(const area& b1, const area& b2) const {
        auto top_right_x = std::max(b1.top_right_.x(), b2.top_right_.x());
        auto top_right_y = std::max(b1.top_right_.y(), b2.top_right_.y());
        auto bottom_left_x = std::min(b1.bottom_left_.x(), b2.bottom_left_.x());
        auto bottom_left_y = std::min(b1.bottom_left_.y(), b2.bottom_left_.y());

        return area{top_right_x, top_right_y, bottom_left_x, bottom_left_y};
    }

    area intersection(const area& b1, const area& b2) const {
        auto top_right_x = std::min(b1.top_right_.x(), b2.top_right_.x());
        auto top_right_y = std::min(b1.top_right_.y(), b2.top_right_.y());
        auto bottom_left_x = std::max(b1.bottom_left_.x(), b2.bottom_left_.x());
        auto bottom_left_y = std::max(b1.bottom_left_.y(), b2.bottom_left_.y());

        return area{top_right_x, top_right_y, bottom_left_x, bottom_left_y};
    }

    bool contains(const coordinate& c) {
        return (top_right_.x() > c.x() &&
                top_right_.y() > c.y() &&
                bottom_left_.x() < c.x() &&
                bottom_left_.y() < c.y());
    }

    bool contains(const area& a) const {
        return (top_right_.x() > a.top_right_.x() &&
                top_right_.y() > a.top_right_.y() &&
                bottom_left_.x() < a.bottom_left_.x() &&
                bottom_left_.y() < a.bottom_left_.y());
    }

    bool intersects(const area& a) const {
//        return (bottom_left_.y() < a.top_right_.y() ||
//                bottom_left_.x() < a.top_right_.x() ||
//                top_right_.x() > a.bottom_left_.x() ||
//                top_right_.y() > a.bottom_left_.y());
    }

    area operator +(const area& a2) {
        return {
            bottom_left_ + a2.bottom_left(), top_right_ + a2.top_right()
        };
    }

private:
    coordinate bottom_left_, top_right_;
};
}

#endif // BOUNDING_BOX_H
