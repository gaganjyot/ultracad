#ifndef COORDINATE_H
#define COORDINATE_H

#include <algorithm>

namespace util {
struct coordinate {

    explicit coordinate(double x = 0, double y = 0) : x_(x), y_(y) {}

    double x() const {
        return x_;
    }

    double y() const {
        return y_;
    }

    coordinate operator +(const coordinate& c) const {
        return coordinate{x_ + c.x(), y_ + c.y()};
    }

    coordinate operator +(double val) const {
        return coordinate{x_ + val, y_ + val};
    }

    coordinate operator -(const coordinate& c) const {
        return coordinate{x_ - c.x(), y_ - c.y()};
    }

    coordinate operator -(double val) const {
        return coordinate{x_ - val, y_ - val};
    }

    coordinate operator *(double val) const {
        return coordinate{x_ * val, y_ * val};
    }

    coordinate operator /(double val) const {
        return coordinate{x_ / val, y_ / val};
    }

    coordinate operator -() const {
        return coordinate{-x_, -y_};
    }

    coordinate move(coordinate& pos) const {
        return coordinate{x_ + pos.x(), y_ + pos.y()};
    }

    coordinate move(double x, double y) const {
        return coordinate{x_ + x, y_ + y};
    }

    coordinate rotate(double angle, const coordinate& wrt = coordinate{0,0}) const {
        return rotate(to_coordinate(angle), wrt);
    }

    coordinate rotate(const coordinate& angle_vector, const coordinate &wrt = coordinate{0,0}) const {
        auto x = (x_ - wrt.x()) * angle_vector.x() - (y_ - wrt.y()) * angle_vector.y();
        auto y = (x_ - wrt.x()) * angle_vector.y() + (y_ - wrt.y()) * angle_vector.x();

        return coordinate {
            x + wrt.x(), y + wrt.y()
        };
    }

    coordinate scale(double x, double y) const {
        return coordinate{x_ * x, y_ * y};
    }

    coordinate scale(double factor) const {
        return coordinate{x_ * factor, y_ * factor};
    }

    coordinate mirror(coordinate& axis1, coordinate& axis2) const {

    }

    // TODO yet to implement
    coordinate to_coordinate(double angle) const {
        return coordinate{
            std::cos(angle), std::sin(angle)
        };
    }

    double angle() const {
        return std::atan2(y_, x_);
    }

    static coordinate max(coordinate& c1, coordinate& c2) {
        return coordinate {
            std::max(c1.x(), c2.x()), std::max(c1.y(), c2.y())
        };
    }

    static coordinate min(coordinate& c1, coordinate& c2) {
        return coordinate {
            std::min(c1.x(), c2.x()), std::min(c1.y(), c2.y())
        };
    }

private:
    double x_, y_;
};

}

#endif // COORDINATE_H
