#ifndef TYPES_H
#define TYPES_H

#include <cstdint>

template <typename T, typename U>
struct type_gen {

    explicit type_gen(T const& value) : value_(value) {}
    explicit type_gen(T&& value) : value_(std::move(value)) {}
    T& get() { return value_; }
    T const& get() const {return value_; }

private:
    T value_;
};

using entity_id_t = type_gen<uint64_t, struct entity_id_type>;
using layer_id_t = type_gen<uint64_t, struct layer_id_type>;
using meta_id_t = type_gen<uint64_t, struct meta_id_type>;
using level_t = type_gen<uint64_t, struct level_type>;

#endif // TYPES_H
