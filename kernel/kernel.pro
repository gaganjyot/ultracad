TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp

HEADERS += \
    util/coordinate.h \
    util/bounding_box.h \
    util/color.h \
    base/entity.h \
    base/layer.h \
    interface/event_handler.h \
    entities/circle.h \
    entities/line.h \
    entities/point.h \
    document/document.h \
    base/meta.h \
    base/identity.h \
    document/quadtree.h \
    util/types.h \
    base/action.h \
    interface/painter.h \
    maths/intersection_handler.h \
    maths/equations.h \
    maths/snap_handler.h \
    util/generic_visitor.h \
    actions/line_create_action.h \
    actions/circle_create_action.h \
    actions/point_create_action.h
