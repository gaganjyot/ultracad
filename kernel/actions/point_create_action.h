#ifndef POINT_CREATE_ACTION_H
#define POINT_CREATE_ACTION_H

#include "base/action.h"

struct point_create_action : base::action {
    point_create_action() {}
    ~point_create_action() {}
    void init() override {}
    void process() override {}
    void commit() override {}
    void undo() override {}
    void redo() override {}
    void on_mouse_press(const mouse_event& me) override {}
    void on_mouse_release(const mouse_event& me) override {}
    void on_mouse_double_click(const mouse_event& me) override {}
    void on_mouse_move(const mouse_event& me) override {}
    void on_key_press(const keyboard_event& me) override {}
    void on_key_release(const keyboard_event& me) override {}
};

#endif // POINT_CREATE_ACTION_H
