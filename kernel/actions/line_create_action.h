#ifndef LINE_CREATE_ACTION_H
#define LINE_CREATE_ACTION_H

#include "base/action.h"

struct line_create_action : base::action {
    line_create_action() {}
    ~line_create_action() {}
    void init() override {}
    void process() override {}
    void commit() override {}
    void undo() override {}
    void redo() override {}
    void on_mouse_press(const mouse_event& me) override {}
    void on_mouse_release(const mouse_event& me) override {}
    void on_mouse_double_click(const mouse_event& me) override {}
    void on_mouse_move(const mouse_event& me) override {}
    void on_key_press(const keyboard_event& me) override {}
    void on_key_release(const keyboard_event& me) override {}

};


#endif // LINE_CREATE_ACTION_H
