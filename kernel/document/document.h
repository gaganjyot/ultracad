#ifndef DOCUMENT_H
#define DOCUMENT_H

#include "base/entity.h"
#include "base/meta.h"
#include "base/layer.h"

#include <vector>
#include <memory>

#include "quadtree.h"

namespace storage {

struct document {
    using namespace base;

    virtual void add_entity(entity_csptr e) = 0;
    virtual void remove_entity(entity_id_t e) = 0;
    virtual void update_entity(entity_csptr e) = 0;

    virtual void add_layer(layer_csptr l) = 0;
    virtual void remove_layer(layer_id_t l) = 0;
    virtual void udpate_layer(layer_csptr l) = 0;

    virtual void add_meta(meta_csptr m) = 0;
    virtual void remove_meta(meta_id_t m) = 0;
    virtual void update_meta(meta_csptr m) = 0;

    virtual entity_csptr entity_by_id(uint64_t entity_id) = 0;
    virtual std::vector<entity_csptr> entities_by_layer(std::string layer_name) = 0;
};

}

#endif // DOCUMENT_H
