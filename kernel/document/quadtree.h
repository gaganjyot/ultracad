#ifndef QUADTREE_H
#define QUADTREE_H

#include "util/bounding_box.h"
#include "util/types.h"

#include <vector>
#include <memory>

template<typename T>
struct quad_tree {

    quad_tree(const util::area& area = {{-5000000, -5000000}, {5000000, 5000000}},
              level_t level = level_t{0})
        : area_{area}, level_{level} { }

    void insert(T entity) {

    }

    void remove(entity_id_t id) {

    }

    std::vector<T> all_entities() const {

    }

    std::vector<T> entities_at_level(level_t level) const {

    }

    std::vector<T> entities_within_levels(level_t max_level, level_t min_level) const {

    }

    std::vector<T> entities_within_area(const util::area& area) const {

    }

    std::vector<util::area> split_area() {
        auto height_by_2 = area_.height() / 2;
        auto width_by_2 = area_.width() / 2;

        auto start_p1 = area_.bottom_left();
        auto start_p2 = util::coordinate{area_.bottom_left().x(), area_.bottom_left().y() + height_by_2};
        auto start_p3 = util::coordinate{area_.bottom_left().x() + width_by_2, area_.bottom_left().y() + height_by_2};
        auto start_p4 = util::coordinate{area_.bottom_left().x() + width_by_2, area_.bottom_left().y()};

        auto end_p1 = start_p3;
        auto end_p2 = util::coordinate{area_.top_right().x() + width_by_2, area_.bottom_left().y()};
        auto end_p3 = area_.top_right();
        auto end_p4 = util::coordinate{area_.top_right().x(), area_.bottom_left().y() + height_by_2};

        return {
            util::area{start_p1, end_p1},
            util::area{start_p2, end_p2},
            util::area{start_p3, end_p3},
            util::area{start_p4, end_p4}
        };
    }

private:

    uint8_t quadrant_selector(const util::area& box) const {

    }

    void get_all_entities_internal(std::vector<T>& vec) {

    }

    std::array<std::unique_ptr<quad_tree>, 4> nodes_;
    std::vector<T> entities_;
    level_t level_;
    std::vector<entity_id_t> entity_id_list_;
    util::area area_;
};

#endif // QUADTREE_H
