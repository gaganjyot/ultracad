#ifndef PAINTER_H
#define PAINTER_H

#include <cstdint>

namespace interface {
class painter {
    virtual void set_color(uint8_t red, uint8_t green, uint8_t blue, uint8_t alpha = 0) = 0;
    virtual void set_thickness(uint8_t thickness) = 0;
    virtual void draw_line(double x1, double y1, double x2, double y2) = 0;
    virtual void draw_circle(double x, double y, double radius) = 0;
    virtual void draw_arc(double x, double y, double radius, double start_angle, double end_angle) = 0;
    virtual void draw_point(double x, double y) = 0;
};
}

#endif // PAINTER_H
