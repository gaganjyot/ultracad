#ifndef EVENT_HANDLER_H
#define EVENT_HANDLER_H

#include <cstdint>

struct point { int x_, y_; };

struct mouse_event {
    point local_pos_;
    mouse_event_type e_type_;
    point global_pos_;
};

struct scroll_event {
    double delta_;
};

enum class mouse_event_type {
    PRIMARY_CLICK = 0x00001,
    PRIMARY_DOUBLE_CLICK = 0x00002,
    SECONDARY_CLICK = 0x00004,
    SECONDARY_DOUBLE_CLICK = 0x00010,
    MOUSE_MOVE = 0x00020
};

enum class keyboard_event_type {
    KEY_RELEASE = 0x00001,
    KEY_PRESS = 0x00002,
    SHIFT_MODIFIER = 0x00004,
    CTRL_MODIFIER = 0x00010,
    ALT_MODIFIER = 0x000020
};

struct keyboard_event {
    uint8_t key_;
    keyboard_event_type e_type_;
};

#endif // EVENT_HANDLER_H
