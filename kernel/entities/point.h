#ifndef POINT_H
#define POINT_H

#include "base/entity.h"

struct point : base::entity {
    using namespace util;

    point(double x = 0, double y = 0) : point_({x, y}) {}

    std::shared_ptr<entity> move(double x, double y) const override {
        return std::make_shared<point>{
            point_.move(x, y)
        };
    }

    std::shared_ptr<entity> move(const coordinate& pos) const override {
        return std::make_shared<point>{
            point_.move(pos)
        };
    }

    std::shared_ptr<entity> copy(double x, double y) const override {
        return std::make_shared<point>{
            point_.copy(x, y)
        };
    }

    std::shared_ptr<entity> copy(const coordinate& pos) const override {
        return std::make_shared<point>{
            point_.copy(pos)
        };
    }

    std::shared_ptr<entity> scale(double factor) const override {
        return std::make_shared<point>{
            point_.scale(factor)
        };
    }

    std::shared_ptr<entity> scale(double x, double y) const override {
        return std::make_shared<point>{
            point_.scale(x, y)
        };
    }

    std::shared_ptr<entity> rotate(double angle, const coordinate& wrt = {0, 0}) const override {
        return std::make_shared<point>{
            point_.rotate(angle), wrt
        };
    }

    std::shared_ptr<entity> rotate(const coordinate& angle_vector, const coordinate& wrt = {0, 0}) const override {
        return std::make_shared<point>{
            point_.rotate(angle_vector), wrt
        };
    }

    std::shared_ptr<entity> mirror(const coordinate& axis1, const coordinate& axis2) const override {
        return std::make_shared<point>{
            point_.mirror(axis1, axis2)
        };
    }

    coordinate nearest_point_on_entity() const override {
        return point_;
    }

    coordinate nearest_point_on_path() const override {
        return point_;
    }

    area bounding_box() const override {
        return area{
            point_, point_
        };
    }


private:
    coordinate point_;
};

#endif // POINT_H
