#ifndef LINE_H
#define LINE_H

#include "base/entity.h"

struct line : base::entity {
    using namespace util;

    line(const coordinate& start = {0, 0}, const coordinate& end = {0, 0})
        : start_(start), end_(end) {

    }

    coordinate start() const {
        return start_;
    }

    coordinate end() const {
        return end_;
    }

    std::shared_ptr<entity> move(double x, double y) const override {
        return std::make_shared<line>{
            {start_.x() + x, start_.y() + y},
            {end_.x() + x, end_.y() + y}
        };
    }

    std::shared_ptr<entity> move(const coordinate& pos) const override {
        return std::make_shared<line>{
            start_ + pos, end_ + pos
        };
    }

    std::shared_ptr<entity> copy(double x, double y) const override {
        return std::make_shared<line>{
            {start_.x() + x, start_.y() + y},
            {end_.x() + x, end_.y() + y}
        };
    }

    std::shared_ptr<entity> copy(const coordinate& pos) const override {
        return std::make_shared<line>{
            start_ + pos, end_ + pos
        };
    }

    std::shared_ptr<entity> scale(double factor) const override {
        return std::make_shared<line>{
            start_ * factor, end_  * factor
        };
    }

    std::shared_ptr<entity> scale(double x, double y) const override {
        return std::make_shared<line>{
            start_ * x, end_  * y
        };
    }

    std::shared_ptr<entity> rotate(double angle, const coordinate& wrt = {0, 0}) const override {
        return std::make_shared<line>{
            start_.rotate(angle, wrt), end_.rotate(angle, wrt)
        };
    }

    std::shared_ptr<entity> rotate(const coordinate& angle_vector, const coordinate& wrt = {0, 0}) const override {
        return std::make_shared<line>{
            start_.rotate(angle_vector, wrt), end_.rotate(angle_vector, wrt)
        };
    }

    std::shared_ptr<entity> mirror(const coordinate& axis1, const coordinate& axis2) const override {
        return std::make_shared<line>{
            start_.mirror(axis1, axis2), end_.mirror(axis1, axis2)
        };
    }

    coordinate nearest_point_on_entity() const override {

    }

    coordinate nearest_point_on_path() const override {

    }

    area bounding_box() const override {
        return area{
            coordinate::min(start_, end_), coordinate::max(start_, end_)
        };
    }


private:
    coordinate start_, end_;
};

#endif // LINE_H
