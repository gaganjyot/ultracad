#ifndef CIRCLE_H
#define CIRCLE_H

#include <base/entity.h>

struct circle : base::entity {
    using namespace util;

    circle(const coordinate& center = {0, 0}, double radius = 0)
        : center_(center), radius_(radius) {

    }

    coordinate center() {
        return center_;
    }

    coordinate radius() {
        return radius_;
    }

    std::shared_ptr<entity> move(double x, double y) const override {
        return std::make_shared<circle>{
            coordinate{center_.x() + x, center_.y() + y},
            radius_
        };
    }

    std::shared_ptr<entity> move(const coordinate& pos) const override {
        return std::make_shared<circle>{
            center_ + pos, radius_
        };
    }

    std::shared_ptr<entity> copy(double x, double y) const override {
        return std::make_shared<circle>{
            coordinate{center_.x() + x, center_.y() + y},
            radius_
        };

    }

    std::shared_ptr<entity> copy(const coordinate& pos) const override {
        return std::make_shared<circle>{
            center_ + pos, radius_
        };
    }

    std::shared_ptr<entity> scale(double factor) const override {
        return std::make_shared<circle>{
            center_ * factor, radius_ * factor
        };
    }

    std::shared_ptr<entity> scale(double x, double y) const override {

    }

    std::shared_ptr<entity> rotate(double angle, const coordinate& wrt = {0, 0}) const override {
        return std::make_shared<circle>{
            center_.rotate(angle, wrt), radius_
        };
    }

    std::shared_ptr<entity> rotate(const coordinate& angle_vector, const coordinate& wrt = {0, 0}) const override {
        return std::make_shared<circle>{
            center_.rotate(angle_vector, wrt), radius_
        };
    }

    std::shared_ptr<entity> mirror(const coordinate& axis1, const coordinate& axis2) const override {
        return std::make_shared<circle>{
            center_.mirror(axis1, axis2), radius_
        };
    }

    coordinate nearest_point_on_entity() const override {

    }

    coordinate nearest_point_on_path() const override {

    }

    area bounding_box() const override {
        return area{
            center_ - radius_, center_ + radius_
        };
    }


private:
    coordinate center_;
    double radius_;
};

#endif // CIRCLE_H
