#ifndef META_H
#define META_H

#include <memory>

namespace base {

struct meta;
using meta_sptr = std::shared_ptr<meta>;
using meta_csptr = std::shared_ptr<const meta>;

struct meta {

};
}

#endif // META_H
