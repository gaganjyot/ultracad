#ifndef ACTION_H
#define ACTION_H

#include "interface/event_handler.h"

namespace base {
struct action
{
    action() {}
    virtual ~action() {}
    virtual void init() = 0;
    virtual void process() = 0;
    virtual void commit() = 0;
    virtual void undo() = 0;
    virtual void redo() = 0;
    virtual void on_mouse_press(const mouse_event& me) = 0;
    virtual void on_mouse_release(const mouse_event& me) = 0;
    virtual void on_mouse_double_click(const mouse_event& me) = 0;
    virtual void on_mouse_move(const mouse_event& me) = 0;
    virtual void on_key_press(const keyboard_event& ke) = 0;
    virtual void on_key_release(const keyboard_event& ke) = 0;
};
}

#endif // ACTION_H
