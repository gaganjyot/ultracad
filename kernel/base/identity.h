#ifndef IDENTITY_H
#define IDENTITY_H

#include <cstdint>
#include <atomic>
#include <util/types.h>

namespace base {
struct entity_id {
    entity_id() {
        id_++;
    }

    entity_id_t id() const {
        return entity_id_t{id_};
    }

private:
    static std::atomic_uint64_t id_;
};

std::atomic_uint64_t entity_id::id_ = 0;
}

#endif // IDENTITY_H
