#ifndef LAYER_H
#define LAYER_H

#include <memory>
#include <string>

namespace base {

struct layer;
using layer_sptr = std::shared_ptr<layer>;
using layer_csptr = std::shared_ptr<const layer>;

struct layer {

private:
    std::string layer_name_;
};
}

#endif // LAYER_H
