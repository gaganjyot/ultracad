#ifndef ENTITY_H
#define ENTITY_H

#include <memory>
#include "util/coordinate.h"
#include "util/bounding_box.h"

namespace base {

struct entity;
using entity_sptr = std::shared_ptr<entity>;
using entity_csptr = std::shared_ptr<const entity>;

struct entity {
    using namespace util;
    virtual entity_csptr move(double x, double y) const = 0;
    virtual entity_csptr move(const coordinate& pos) const = 0;
    virtual entity_csptr copy(double x, double y) const = 0;
    virtual entity_csptr copy(const coordinate& pos) const = 0;
    virtual entity_csptr scale(double factor) const = 0;
    virtual entity_csptr scale(double x, double y) const = 0;
    virtual entity_csptr rotate(double angle, const coordinate& wrt = {0, 0}) const = 0;
    virtual entity_csptr rotate(const coordinate& angle_vector, const coordinate& wrt = {0, 0}) const = 0;
    virtual entity_csptr mirror(const coordinate& axis1, const coordinate& axis2) const = 0;

    virtual coordinate nearest_point_on_entity() const = 0;
    virtual coordinate nearest_point_on_path() const = 0;
    virtual area bounding_box() const = 0;

};

}

#endif // ENTITY_H
